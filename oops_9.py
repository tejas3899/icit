class Student:
    def __init__(self, n='Tejas', rn=74):
        self.name = n;
        self.rollnumber = rn;

    def setData(self):
        self.name = input('Enter a name of student:');
        self.rollnumber = input('Enter a roll number of student:')


    def getData(self):
        print('The name of student is ={}'.format(self.name))
        print('The roll number of student is ={}'.format(self.rollnumber))

a1 = Student();

#a1.setData();
a1.getData();
