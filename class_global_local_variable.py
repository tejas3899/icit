class Student:
    
    #consructor

    def __init__(self, n = 'x', rn = 1):
        self.name = n;
        self.rollnumber = rn;

    def getData(self):
        print('From Get Data: Student1 ={}\n Roll No. ={}'.format(self.name, self.rollnumber));

    def __repr__(self):
        return 'Printing from Repr function: Student1 ={} Roll No. ={}\n'.format(self.name, self.rollnumber)
        
#Objects
        
a1 = Student();
a2 = Student('Vijay',77)
a3 = Student('Mukesh',78)

listStudent = [a1,a2,a3]

'''
a1.getData();
a2.getData();
a3.getData();
'''

#print(listStudent)


for x in listStudent:
    print('Students Data:')
    #print(x)
    x.getData();
    


