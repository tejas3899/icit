class First:
    def first(self):
        print('I am first')

class Second:
    def second(self):
        print('I am second')

class Third(First,Second):
    def third(self):
        super().second();
        super().first();
        print('I am third')


a1 = Third();
a1.third()
