list = [1,2,3,4]


print(id(list[0]));
print(id(list[1]));
print(id(list[2]));
print(id(list[3]));


x = 10;
y = x;

print('Id of x is',id(x))
print('Id of y is',id(y))

z = 10
print('Id of z is',id(z))

z = 20
print('Id of z is',id(z))


x = 10;

if x is 10:
    print('yes')

else:
    print('no')

x = 10

if x is not 10:
    print('not')

else:
    print('yes')
