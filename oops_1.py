class Student:
        
    def __init__(self, a=1, n='', r=1):
        self.name = n;
        self.age = a;
        self.rollno = r;

    def setData(self, nm, ag, rn):
        self.name = nm;
        self.age = ag;
        self.rollno = rn;
        

    def getdata(self):
        print('Name ={},Age ={},Roll number={}'.format(self.name,self.age,self.rollno))



s1 = Student(22,'Ashish',1);

s1.getdata();

s2 = Student(25,'Ramesh',2);

s2.getdata();

s3 = Student(28,'Mahesh',3);

s3.getdata();

s4 = Student(31,'Suresh',4);

s4.getdata();

s5 = Student();
s5.getdata();
s5.setData("Shailesh", 25, 123);
s5.getdata();
